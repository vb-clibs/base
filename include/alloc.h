#ifndef VB_ALLOCATOR_H
#define VB_ALLOCATOR_H


/*
 * These macros are used in order to change the allocator at compile time.
 * Just define the following macros to what you want to use.
 */
#ifndef vb_malloc
#define vb_malloc malloc
#endif

#ifndef vb_calloc
#define vb_calloc calloc
#endif

#ifndef vb_realloc
#define vb_realloc realloc
#endif

#ifndef vb_free
#define vb_free	free
#endif


/*
 *
 */
void *vb_sec_malloc(size_t size, void (*err_handle)());

/*
 *
 */
void *vb_sec_calloc(size_t size, size_t numb, void (*err_handle)());

/*
 *
 */
void *vb_sec_realloc(void *mem,  size_t size, void (*err_handle)());

/*
 *
 */
void *vb_def_malloc(size_t x);

/*
 *
 */
void *vb_def_calloc(size_t x, size_t n);

/*
 *
 */
void *vb_def_realloc(void *p, size_t x);

/*
 *
 */
static inline void vb_default_err_handler()
{
	puts("Memory allocation failed!");
	exit(FAILURE);
}


#ifdef VB_NAMESPACES
struct vb_allocator_namespace {
    void (*free)(void *);
    void *(*malloc)(size_t);
    void *(*calloc)(size_t, size_t);
	void *(*realloc)(void *, size_t);
	void (*errhandler)();
};

static struct vb_allocator_namespace
VB_Allocator = {
    .free    = &vb_free,
    .malloc  = &vb_def_malloc,
	.calloc  = &vb_def_calloc,
	.realloc = &vb_def_realloc,
	.errhandler = &vb_default_err_handler,
};
#endif
#endif /* VB_ALLOCATOR_H */
