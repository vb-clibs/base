#ifndef VB_VARARG_H
#define VB_VARARG_H


/*
 * Thanks to: Rodrigo Dias Cruz <rodrigodc@gmail.com>
 *
 * arglen (helper) - given a string #__VA_ARGS__, returns
 *                   the number of optional arguments it
 *                   contains
 *
 * For example:
 * #define funct(...) __funct(vb_arglen(#__VA_ARGS__), ##__VA_ARGS__)
 *
 */
int vb_arglen(char *args);


#endif /* VB_VARARG_H */
