#ifndef VB_BASE_H
#define VB_BASE_H

#define VB_BASE_VERSION "1.0.0"


#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>


#define     VB_SUCCESS     		true
#define     VB_FAILURE     		false

#define     VB_SIZE_POINTER    sizeof(void *)
#define     VB_SIZE_CHAR       sizeof(char)
#define     VB_SIZE_INT        sizeof(int)
#define     VB_SIZE_SHORT      sizeof(short)
#define     VB_SIZE_DOUBLE     sizeof(double)
#define     VB_SIZE_FLOAT      sizeof(float)
#define     VB_SIZE_LONG       sizeof(long)
#define     VB_SIZE_LLONG      sizeof(long long)

#define     VB_SIZE_BYTE       0x01
#define     VB_SIZE_WORD       0x02


typedef uint8_t  vb_byte_t;
typedef uint16_t vb_word_t;

typedef long long vb_long_t;
typedef unsigned long long vb_ulong_t;

/*
 * thanks to wayne@csri.utoronto.ca
 */
typedef union _vb_foint_t {
	char c;
	int i;
	float f;
	void *v;
} vb_foint_t;

#endif /* VB_BASE_H */
