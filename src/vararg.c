#include <vararg.h>


int vb_arglen(char *args)
{
    char delim;
    int num = 1;
    bool parse = true;

    if(*args == '\0') {
        return 0;
    }

    while(*args != '\0') {
        if(parse) {
            if((*args == '"') || (*args == '\'')) {
                delim = *args;
                parse = false;
            }
            else if(*args == ',') {
                num++;
            }
        }
        else if(*args == delim) {
            parse = true;
        }

		args++;
    }

    return num;
}
