#include <alloc.h>


void *vb_sec_malloc(size_t size, void (*err_handle)())
{
	void *block = vb_malloc(size);

	if(block == NULL) {
		err_handle();
		return NULL;
	}

	return block;
}


void *vb_sec_calloc(size_t size, size_t numb, void (*err_handle)())
{
	void *block = vb_calloc(size, numb);

	if(block == NULL) {
		err_handle();
		return NULL;
	}

	return block;
}


void *vb_sec_realloc(void *mem, size_t size, void (*err_handle)())
{
	void *block = vb_realloc(mem, size);

	if(block == NULL) {
		err_handle();
		return NULL;
	}

	return block;
}


void *vb_def_malloc(size_t x) {
	return vb_sec_malloc(x, VB_Allocator.errhandler);
}


void *vb_def_calloc(size_t x, size_t n) {
	return vb_sec_calloc(x, n, VB_Allocator.errhandler);
}


void *vb_def_realloc(void *p, size_t x) {
	return vb_sec_realloc(p, x, VB_Allocator.errhandler);
}
